﻿using System.Windows;

namespace _30._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        BankRekening bankstatement = new BankRekening(1000.00, 2.3);
        Figuur figure = new Figuur(30, 10);

        private void btnInterest_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(bankstatement.ToString());
        }

        private void btnOppervlakte_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(figure.ToString());
        }
    }
}
