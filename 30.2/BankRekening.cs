﻿namespace _30._2
{


    class BankRekening : InterfaceObject
    {
        private double _saldo;
        private double _rentevoet;



        public BankRekening(double saldo, double rentevoet)
        {
            Saldo = saldo;
            Rentevoet = rentevoet;
        }

        public double Saldo { get; set; }
        public double Rentevoet { get; set; }

        public double Bereken() => Saldo * Rentevoet;




        public override string ToString()
        {
            return "De interest van de rekening met saldo €" + " " + Saldo + " " + "en rentevoet " + " " + Rentevoet + " " + "is" + " " + Bereken().ToString();
        }

    }
}
