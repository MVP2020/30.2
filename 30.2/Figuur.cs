﻿namespace _30._2
{
    class Figuur : InterfaceObject
    {
        private double _breedte;
        private double _hoogte;

        public double Breedte { get; set; }
        public double Hoogte { get; set; }


        public Figuur(double hoogte, double breedte)
        {
            Breedte = 10;
            Hoogte = 30;
        }

        public double Bereken() => Breedte * Hoogte;




        public override string ToString()
        {
            return "De oppervlakte van de figuur met hoogte" + " " + Hoogte + " " + "en breedte" + " " + Breedte + " " + "is" + " " + Bereken().ToString();
        }




    }
}
